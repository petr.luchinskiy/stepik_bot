from aiogram import Bot, Dispatcher, executor
from aiogram.types import InlineKeyboardMarkup, InlineKeyboardButton, Message


# Вместо BOT TOKEN HERE нужно вставить токен вашего бота, полученный у @BotFather
API_TOKEN: str = '5945111885:AAE9xFnbq7-pjCPsUSVT-W_aaJ55JLbXBSg'

# Создаем объекты бота и диспетчера
bot: Bot = Bot(token=API_TOKEN)
dp: Dispatcher = Dispatcher(bot)

# Создаем объект инлайн-клавиатуры
keyboard: InlineKeyboardMarkup = InlineKeyboardMarkup()

# Создаем объекты инлайн-кнопок
url_button_1: InlineKeyboardButton = InlineKeyboardButton(
                                        text='Курс "Телеграм-боты на Python и AIOgram"',
                                        url='https://stepik.org/120924')
url_button_2: InlineKeyboardButton = InlineKeyboardButton(
                                        text='Документация Telegram Bot API',
                                        url='https://core.telegram.org/bots/api')

# Добавляем кнопки в клавиатуру методом add
keyboard.add(url_button_1).add(url_button_2)


# Этот хэндлер будет срабатывать на команду "/start" и отправлять в чат клавиатуру
async def process_start_command(message: Message):
    await message.answer(text='Это инлайн-кнопки с параметром "url"',
                         reply_markup=keyboard)


# Регистрируем хэндлер
dp.register_message_handler(process_start_command, commands='start')


if __name__ == '__main__':
    executor.start_polling(dp, skip_updates=True)