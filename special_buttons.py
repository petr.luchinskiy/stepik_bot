from aiogram.types import ReplyKeyboardMarkup, KeyboardButton, KeyboardButtonPollType


# Создаем объект клавиатуры
keyboard: ReplyKeyboardMarkup = ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True)

# Добавляем кнопки в клавиатуру
keyboard.add(KeyboardButton(text='Отправить телефон', request_contact=True))
keyboard.add(KeyboardButton(text='Отправить геолокацию', request_location=True))
keyboard.add(KeyboardButton(text='Создать опрос/викторину', request_poll=KeyboardButtonPollType()))


# Этот хэндлер будет срабатывать на команду "/start"
async def process_start_command(message: types.Message):
    await message.answer('Экспериментируем со специальными кнопками', reply_markup=keyboard)