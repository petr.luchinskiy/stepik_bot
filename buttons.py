from aiogram import Bot, Dispatcher, executor, types
from aiogram.types import ReplyKeyboardMarkup, ReplyKeyboardRemove, KeyboardButton


# Вместо BOT TOKEN HERE нужно вставить токен вашего бота, полученный у @BotFather
API_TOKEN: str = '5945111885:AAE9xFnbq7-pjCPsUSVT-W_aaJ55JLbXBSg'

# Создаем объекты бота и диспетчера
bot: Bot = Bot(token=API_TOKEN)
dp: Dispatcher = Dispatcher(bot)

# Создаем объект клавиатуры
keyboard: ReplyKeyboardMarkup = ReplyKeyboardMarkup()

# Создаем объекты кнопок
button_1: KeyboardButton = KeyboardButton('Собак 🦮')
button_2: KeyboardButton = KeyboardButton('Огурцов 🥒')

# Добавляем кнопки в клавиатуру методом add
keyboard.add(button_1, button_2)


# Этот хэндлер будет срабатывать на команду "/start" и отправлять в чат клавиатуру
async def process_start_command(message: types.Message):
    await message.answer('Чего кошки боятся больше?', reply_markup=keyboard)


# Этот хэндлер будет срабатывать на ответ "Собак 🦮" и удалять клавиатуру
async def process_dog_answer(message: types.Message):
    await message.answer('Да, несомненно, кошки боятся собак. Но вы видели как они боятся огурцов?', reply_markup=ReplyKeyboardRemove())


# Этот хэндлер будет срабатывать на ответ "Огурцов 🥒" и удалять клавиатуру
async def process_cucumber_answer(message: types.Message):
    await message.answer('Да, иногда кажется, что огурцов кошки боятся больше', reply_markup=ReplyKeyboardRemove())

'''
# Создаем объект клавиатуры
keyboard: ReplyKeyboardMarkup = ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True, row_width=4)

# Добавляем кнопки в клавиатуру
keyboard.add(f'Кнопка 1')
keyboard.add(f'Кнопка 2', f'Кнопка 3')
keyboard.add(f'Кнопка 4', f'Кнопка 5', f'Кнопка 6')
keyboard.add(f'Кнопка 7', f'Кнопка 8', f'Кнопка 9', f'Кнопка 10')

# Добавляем кнопки в клавиатуру
keyboard.add('Кнопка 1')
keyboard.insert('Кнопка insert 1')
keyboard.insert('Кнопка insert 2')
keyboard.insert('Кнопка insert 3')
keyboard.insert('Кнопка insert 4')

Метод insert, в отличие от методов add и row, принимает всего одну кнопку и добавляет ее в конец ряда, если в ряду для
 нее есть место, то есть позволяет значение параметра row_width. Если места нет, то кнопка переносится на следующую строку.
'''
# Регистрируем хэндлеры
dp.register_message_handler(process_start_command, commands='start')
dp.register_message_handler(process_dog_answer, text='Собак 🦮')
dp.register_message_handler(process_cucumber_answer, text='Огурцов 🥒')


if __name__ == '__main__':
    executor.start_polling(dp, skip_updates=True)