from aiogram import Bot, Dispatcher, executor, types


# Вместо BOT TOKEN HERE нужно вставить токен вашего бота, полученный у @BotFather
API_TOKEN: str = '5945111885:AAE9xFnbq7-pjCPsUSVT-W_aaJ55JLbXBSg'

# Создаем объекты бота и диспетчера
bot: Bot = Bot(token=API_TOKEN)
dp: Dispatcher = Dispatcher(bot)


# Создаем асинхронную функцию
async def set_main_menu(dp: Dispatcher):
    # Создаем список с командами для кнопки menu
    main_menu_commands = [
        types.BotCommand(command='/help', description='Справка по работе бота'),
        types.BotCommand(command='/support', description='Поддержка'),
        types.BotCommand(command='/contacts', description='Другие способы связи'),
        types.BotCommand(command='/payments', description='Платежи')
    ]
    await dp.bot.set_my_commands(main_menu_commands)


if __name__ == '__main__':
    executor.start_polling(dp, skip_updates=True, on_startup=set_main_menu)